#!/bin/bash

#Uso: sh consulta <string consulta>"

function ayuda {\
  echo " Este script realiza una búsqueda en la BD"
  echo " Uso: ./consulta.sh <string consulta>"
  echo "================================================================"
}

if [ $# -lt 1 ]; then
  ayuda
exit 1
fi


#Variables
POSTING=BBDD/Posting/
INDEX=BBDD/Index/
STOP=BBDD/StopList/StopList-En.txt
SEARCH=build/search
OUTPUT=output/

#consulta <string consulta> <directorio de PostingFiles> <directorio de archivos indice> <stoplist> <directorio de salida> 
INICIO=$(date +"%s")
$SEARCH "$1" $POSTING $INDEX $STOP $OUTPUT
FIN=$(date +"%s")

TIEMPO=$(($FIN-$INICIO))

echo "La consulta ha tardado $(($TIEMPO / 60)) minutos y $(($TIEMPO % 60)) segundos."