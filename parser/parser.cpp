#include <iostream>

#include <QDir>

#include <StopList.h>
#include <Document.h>
#include <DocumentXML.h>


using namespace std;

int main(int argc, char *argv[])
{
    // First argument: Input folder
    // Second argument: Output folder
    // Third argument: Stoplist


    if(argc != 4){
        cerr << "Number of parameters incorrect" << endl;
        return(1);
    }

    QDir input_folder(argv[1]);
    QDir output_folder(argv[2]);
    StopList stoplist(argv[3]);

    if(!input_folder.exists()){
        cerr << "Input folder doesn't exist" << endl;
        return(1);
    }

    input_folder.setFilter(QDir::Files |QDir::NoSymLinks);
    input_folder.setSorting(QDir::Name);

    QFileInfoList fileList = input_folder.entryInfoList();
    QFileInfo file;

    for(int i=0; i<fileList.size(); i++){
        file=fileList.at(i);
        Document doc(file.absoluteFilePath(), &stoplist);
        DocumentXML xml(doc, output_folder.absolutePath().append("/"));
    }

    cout << "Success!" << endl;
    return 0;
}

