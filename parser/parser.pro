TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += qt

#QMAKE_CXXFLAGS_RELEASE += -std=c++0x
#QMAKE_CXXFLAGS_RELEASE -= -O2
#QMAKE_CXXFLAGS_RELEASE += -O3
#QMAKE_LFLAGS_RELEASE -= -O1
#QMAKE_LFLAGS_RELEASE += -O3

INCLUDEPATH = ../common

SOURCES += \
    parser.cpp \
    ../common/StopList.cpp \
    ../common/DocumentXML.cpp \
    ../common/Document.cpp

HEADERS += \
    ../common/StopList.h \
    ../common/DocumentXML.h \
    ../common/Document.h

