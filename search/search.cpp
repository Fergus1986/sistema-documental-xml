#include <iostream>

#include <QDir>
#include <QString>
#include <QStringList>
#include <QList>
#include <QMap>

#include <StopList.h>
#include <Question.h>
#include <DocumentXML.h>
#include <PostingDocument.h>
#include <PostingDocumentXML.h>
#include <PostingFile.h>
#include <PostingFileXML.h>
#include <Result.h>
#include <ResultXML.h>

using namespace std;

int main(int argc, char *argv[])
{
    // First argument: Search query string
    // Second argument: Input folder where is the Posting DB
    // Third argument: Input folder where is Index DB
    // Forth argument: Stoplist
    // Fifth argument: Output folder

    if(argc < 6){
        cerr << "Number of parameters incorrect" << endl;
        cerr << argc << " -- Too few arguments" << endl;

//        for(int i=0; i<argc; i++){
//            cerr << argv[i] << endl;
//        }

        return(1);
    }else if(argc > 6){
        cerr << "Number of parameters incorrect" << endl;
        cerr << argc << " -- Too many arguments" << endl;
        return(1);
    }

    QDir input_folder_posting(argv[2]);
    QDir input_folder_index(argv[3]);
    StopList stoplist(argv[4]);
    QDir output_folder(argv[5]);

    if(!input_folder_posting.exists()){
        cerr << "Input folder of Posting DB doesn't exist" << endl;
        return(1);
    }

    if(!input_folder_index.exists()){
        cerr << "Input folder of Index DB doesn't exist" << endl;
        return(1);
    }

    if(!output_folder.exists()){
        cerr << "Output folder doesn't exist" << endl;
        return(1);
    }

    // PostingFile DB
    input_folder_posting.setFilter(QDir::Files |QDir::NoSymLinks);
    input_folder_posting.setSorting(QDir::Name);

    // Index DB
    input_folder_index.setFilter(QDir::Files |QDir::NoSymLinks);
    input_folder_index.setSorting(QDir::Name);

    PostingDocumentXML postingDocumentXML(input_folder_posting.absolutePath().append("/"), "postingDocument");
    PostingDocument* postingDocument = postingDocumentXML.getPostingDocument();

    // Create a Document with the query
    QString query(argv[1]);
//    std::cout << "Query: " << query.toStdString() << std::endl;

    Question question (query, &stoplist);
    DocumentXML questionXML(question, output_folder.absolutePath().append("/"));


    // All the words from the query
    QList<QString> words = question.getRelevanceTable().keys();

    // All the PostingFiles that contains words from the query
    QMap<QString, PostingFile*> postingFiles;

    // All the documentIndexes that contains words from the query
    QMap<int, QString> documentIndexes;

    // All the documents that contains words from the query
    QMap<QString, Document*> documents;

    //Obtain all documentIndexes
    QString doc;
    int docIndex;
    QString word;
    QString letter;
    QList<PostingInfo> postingInfo;
    for(int i=0; i<words.size(); i++){
        word = words.at(i);
        letter = word.at(0);

        if(!postingFiles.contains(letter)){
            postingFiles.insert(letter, PostingFileXML(input_folder_posting.absolutePath().append("/"), letter).getPostingFile());
        }
        postingInfo = postingFiles.value(letter)->findWord(word);
        QList<PostingInfo>::iterator it = postingInfo.begin();
        while(it != postingInfo.end()){
            docIndex = (*it).getDocument();
            doc = postingDocument->getDocumentID(docIndex);
            documentIndexes.insert(docIndex, doc);
            ++it;
        }
    }

    // Obtain all Documents
    QMap<int, QString>::iterator itIndex = documentIndexes.begin();
    while(itIndex!= documentIndexes.end()){
        documents.insert(itIndex.value(), DocumentXML(input_folder_index.absolutePath() + "/" + itIndex.value() + ".xml").getDocument());
        ++itIndex;
    }

    // Calculate Similarity
    Result result(&question);
    QMap<QString, Document*>::iterator itDoc = documents.begin();
    while(itDoc!= documents.end()){
        result.insertDocument(question.calculateSimilarity(itDoc.value()), itDoc.key(), itDoc.value()->getName());
        ++itDoc;
    }

    ResultXML resultXML(output_folder.absolutePath(), "result", &result);


    cout << "Success!" << endl;
    return 0;
}

