TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += qt

#QMAKE_CXXFLAGS_RELEASE += -std=c++0x
#QMAKE_CXXFLAGS_RELEASE -= -O2
#QMAKE_CXXFLAGS_RELEASE += -O3
#QMAKE_LFLAGS_RELEASE -= -O1
#QMAKE_LFLAGS_RELEASE += -O3

INCLUDEPATH = ../common

SOURCES += \
    search.cpp \
    ../common/StopList.cpp \
    ../common/Document.cpp \
    ../common/DocumentXML.cpp \
    ../common/PostingFile.cpp \
    ../common/PostingInfo.cpp \
    ../common/PostingFileXML.cpp \
    ../common/PostingDocument.cpp \
    ../common/PostingDocumentXML.cpp \
    ../common/Question.cpp \
    ../common/Result.cpp \
    ../common/ResultXML.cpp

HEADERS += \
    ../common/StopList.h \
    ../common/Document.h \
    ../common/DocumentXML.h \
    ../common/PostingFile.h \
    ../common/PostingInfo.h \
    ../common/PostingFileXML.h \
    ../common/PostingDocument.h \
    ../common/PostingDocumentXML.h \
    ../common/Question.h \
    ../common/Result.h \
    ../common/ResultXML.h
