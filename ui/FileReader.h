#ifndef FILEREADER_H
#define FILEREADER_H

#include <QObject>
#include <QFile>
#include <QTextStream>

#include <iostream>


class FileReader: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString source
               READ source
               WRITE setSource
               NOTIFY sourceChanged)

public:
    FileReader();
    Q_INVOKABLE QString read();

public slots:
    void setSource(const QString& source);
    QString source();


signals:
    void sourceChanged(const QString& source);
    void error(const QString& msg);

private:
    QString mSource;

};

#endif // FILEREADER_H
