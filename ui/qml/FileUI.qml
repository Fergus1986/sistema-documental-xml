import QtQuick 2.0
import QtWebKit 3.0
import QtQuick.XmlListModel 2.0

Rectangle {
    id: rectangulo
    width: searchbox.width
    height: grid.height - tileMargin
    //principal.height -taskBarTop.height*2 + tileMargin /2
    color: "white"
    anchors.right: exitButton.right
    anchors.leftMargin: 0
    property string contenido: texto.url

    WebView {
        id: texto
        url: contenido
        width: rectangulo.width
        height: rectangulo.height
    }

    Component.onCompleted: rectangulo.state = "no_visible"

    states: [

        State {
            name: "inicial"
            PropertyChanges {
                target: rectangulo
                height:0
                y:-principal.height
            }
        },

        State {
            name: "no_visible"
            PropertyChanges {
                target: rectangulo
                y:-principal.height
            }
        },

        State {
            name: "visible"
            PropertyChanges {
                target: rectangulo
                y:grid.height/2 - (3*tileMargin) + 3
                //taskBarTop.height
            }
        }
    ]

    transitions: [
        Transition {
            from: "*"; to: "visible";
            PropertyAnimation {
                properties: "y"
                duration: 1000
                easing.type: Easing.OutCirc
            }
        },
        Transition {
            from: "*"; to: "no_visible";
            PropertyAnimation {
                properties: "y"
                duration: 1000
                easing.type: Easing.OutCirc
            }
        }
    ]

}

