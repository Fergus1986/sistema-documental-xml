import QtQuick 2.0
import QtQuick.XmlListModel 2.0

import QuestionDocumentUI 1.0

//import FileReader 1.0

Rectangle {
    id: principal
    width: 1366
    height: 768
    color: appBackground

    property string appName: "Sistema Documental XML"

    property int topBarSize: 144
    property int barSize: 120
    property int tileMargin: 12
    property int tileHeaderFontSize: 11
    property int tileDateFontSize: 10
    property int appHeaderFontSize: 36
    property string appBackground: "#262626"
    property string tileBackground: "lightsteelblue"
    property string textColor: "white"

    property double start: 0
    property double finish: 0

    XmlListModel {
        id: xmlModel
        source:"/home/fergus/ARI/output/result.xml"
        query: "/Result/Documents/Document"

        XmlRole { name: "titulo"; query: "Title/string()" }
        XmlRole { name: "relevancia"; query: "Relevance/string()" }
        XmlRole { name: "enlace"; query: "Title/string()" }
        XmlRole { name: "id"; query: "ID/string()" }

    }

    // Top bar
    Rectangle {
        id: topBar
        anchors.left: leftBar.right
        anchors.top: parent.top
        height: topBarSize
        color: appBackground
        Text {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            text: appName
            font.pointSize: appHeaderFontSize;
            color: principal.textColor
        }
    }

    // Left bar
    Rectangle {
        id: leftBar
        anchors.left: parent.left
        anchors.top: parent.top
        opacity: 0
        width: barSize
        height: parent.height
        color: appBackground
    }

    SearchBox {
        id: searchbox
        anchors.left: leftBar.right
        anchors.top: topBar.bottom
        ancho: grid.cellWidth*2 - tileMargin
        alto: grid.cellHeight/2
    }

    QuestionDocumentUI{
        id: questionDocument
    }

    // Related search
    Rectangle{
        id: boton_folder
        anchors.left: searchbox.right
        anchors.leftMargin: tileMargin
        x:searchbox.width+tileMargin
        y:searchbox.y
        width: grid.cellWidth/2 - tileMargin
        height: grid.cellHeight/2
        color:"darkblue"
        Image {
            id: folder
            anchors.centerIn: parent
            source: "/home/fergus/ARI/build/qml/images/folder.png"
            fillMode: Image.PreserveAspectFit
            height: parent.height
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    start = startSearch()
                    questionDocument.setSource(grid.currentItem.data.id)
                    questionDocument.read()
                    xmlModel.reload()
                    finish = finishSearch()
                }
            }
        }
    }

    // Exit Button
    Rectangle{
        id: exitButton
        anchors.left: boton_folder.right
        anchors.leftMargin: tileMargin
        x:searchbox.width+tileMargin+boton_folder.width
        y:searchbox.y
        width: grid.cellWidth/2 - tileMargin
        height: grid.cellHeight/2
        color:"darkblue"

        Image {
            id: info
            anchors.centerIn: parent
            source: "/home/fergus/ARI/build/qml/images/close.png"
            fillMode: Image.PreserveAspectFit
            height: parent.height
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Qt.quit();
                }
            }
        }
    }
    // Grid view
    GridView {
        id: grid
        anchors.left: leftBar.right
        anchors.top: searchbox.bottom
        flow: GridView.TopToBottom
        width: parent.width - 2*leftBar.width
        height: parent.height - topBar.height - bottomBar.height
        interactive: true
        anchors.topMargin: tileMargin /2
        cellHeight: parseInt(grid.height / 4)
        cellWidth: parseInt(cellHeight * 3)
        clip: true
        model: xmlModel
        delegate: Result {property variant data: model}
    }

    // Bottom Bar
    Rectangle {
        id: bottomBar
        anchors.top: grid.bottom
        anchors.left: leftBar.right
        opacity: 100
        width: parent.width - leftBar.width
        height: barSize
        color: appBackground

        Text {
            id: information
            verticalAlignment: Text.AlignVCenter
            text: xmlModel.count + " documentos encontrados en " + (principal.finish-principal.start) + " milésimas de segundo"
            font.pointSize: 14
            color: "white"
            font.italic: true
        }
    }

    // FileReader
    FileUI{
        id: file
        y:taskBarTop.height
        Transition {
            from: "*"; to: "visible";
            PropertyAnimation {
                properties: "y"
                duration: 1000
                easing.type: Easing.OutCirc
            }
        }
    }

    TaskBarTop{
        id: taskBarTop
        onIconPressed: {
            if(file.state=="no_visible"){
                file.state="visible"
                taskBarTop.state="abajo"


            }else{
                file.state="no_visible"
                taskBarTop.state="arriba"

            }
        }
        task_width: grid.cellWidth - tileMargin
    }

    function readFile(){
        file.contenido = grid.currentItem.data.enlace
        file.state="visible"
        taskBarTop.state="abajo"
    }

    function startSearch(){
        var t0 = new Date();
        return t0.valueOf()
    }

    function finishSearch(){
        var t1 = new Date();
        return t1.valueOf()
    }
}
