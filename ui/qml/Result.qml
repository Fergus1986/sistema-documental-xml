import QtQuick 2.0

Rectangle {
    id: delegate
    height: grid.cellHeight
    width: grid.cellWidth
    color: principal.appBackground
    property int tileMargin: principal.tileMargin

    Rectangle {
        id: rectangulo
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: parent.tileMargin/2
        width: parent.width - tileMargin
        height: parent.height - tileMargin
        color: principal.tileBackground

        MouseArea {
            anchors.fill: parent
            onClicked: {
                grid.currentIndex = index
                principal.readFile()
            }
        }

        Text {
            id: titleText
            anchors.top: parent.top
            anchors.topMargin: tileMargin
            anchors.left: parent.left
            anchors.leftMargin: tileMargin
            anchors.right: parent.right
            anchors.rightMargin: tileMargin

            color: principal.textColor
            text: titulo;
            width: parent.width;
            wrapMode: Text.WordWrap
            font {
                bold: false
                pointSize: 12
            }
        }

        Text {
            id: relevance
            anchors.left: parent.left
            anchors.leftMargin: tileMargin
            anchors.bottom: parent.bottom
            anchors.bottomMargin: tileMargin
            anchors.right: parent.right
            anchors.rightMargin: tileMargin

            color: principal.textColor
            text: relevancia
            horizontalAlignment: Text.AlignRight
            width: parent.width;
            font {
                bold: true
                pointSize: 12
            }
        }

        states: [
            State {
                name: "selected"
                when: delegate.GridView.isCurrentItem
                PropertyChanges { target: rectangulo; color: "darkblue" }

            }
        ]

        transitions: Transition {
            from: "*"; to: "selected"; reversible: true
            ColorAnimation { duration: 500 }
        }
    }
}
