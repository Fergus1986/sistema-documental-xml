import QtQuick 2.0

Rectangle {
    id: task
    property string iconPath: ""
    signal iconPressed
    property alias task_width: task.width
    property alias state: task.state
    height: 60
    color: principal.tileBackground
    anchors.left: searchbox.right
    anchors.leftMargin: tileMargin


    Image {
        id: imagen
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: "/home/fergus/ARI/build/qml/images/down.png"


    }
    MouseArea {
        anchors.fill: parent
        onClicked:{
            task.iconPressed();
        }

    }
    states: [

        State {
            name: "abajo"
            PropertyChanges {
                target: imagen
                source: "/home/fergus/ARI/build/qml/images/down.png"
            }
        },

        State {
            name: "arriba"
            PropertyChanges {
                target: imagen
                source: "/home/fergus/ARI/build/qml/images/up.png"
            }
        }
    ]
    transitions: [
        Transition {
            from: "*"; to: "arriba";
            PropertyAnimation { properties: "source"; duration: 200; easing.type: Easing.OutCirc; }
        },
        Transition {
            from: '*'; to: "abajo";
            PropertyAnimation { properties: "source";  duration: 100; easing.type: Easing.OutCirc; }

        }
    ]

}

