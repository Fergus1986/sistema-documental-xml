import QtQuick 2.0

import QuestionUI 1.0

FocusScope {
    id: focusScope
    property alias ancho: focusScope.width
    property alias alto: focusScope.height

    property double start: 0
    property double finish: 0

    Rectangle{
        width: parent.width
        height: parent.height

    }

    Text {
        id: typeSomething
        anchors.fill: parent
        anchors.leftMargin: 10
        verticalAlignment: Text.AlignVCenter
        text: "Enter search terms ..."
        font.pointSize: 12
        color: "gray"
        font.italic: true
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {focusScope.focus = true;}
    }

    TextInput {
        id: textInput
        font.pointSize:12
        anchors {
            left: parent.left
            leftMargin: 8
            right: clear.left
            rightMargin: 8
            verticalCenter: parent.verticalCenter
        }
        focus: true
        selectByMouse: true

        Keys.onReturnPressed:{
            start = startSearch()
            question.setSource(textInput.text)
            question.read()
            xmlModel.reload()
            finish = finishSearch()
        }
    }

    QuestionUI{
        id: question
    }

    Image {
        id: clear
        anchors {
            right: parent.right
            rightMargin: 10
            verticalCenter: parent.verticalCenter
        }
        source: "/home/fergus/ARI/build/qml/images/clear.png"
        smooth: true
        height: parent.height - 20
        fillMode: Image.PreserveAspectFit

        MouseArea {
            anchors.fill: parent
            onClicked: {
                textInput.text = ''
                focusScope.focus = true
                textInput.openSoftwareInputPanel()
            }
        }
    }

    states: State {
        name: "hasText";
        when: textInput.text != ''
        PropertyChanges { target: typeSomething; opacity: 0 }
        PropertyChanges { target: clear; opacity: 1 }
    }

    transitions: [
        Transition {
            from: ""; to: "hasText"
            NumberAnimation { exclude: typeSomething; properties: "opacity" }
        },
        Transition {
            from: "hasText"; to: ""
            NumberAnimation { properties: "opacity" }
        }
    ]

    function startSearch(){
        var t0 = new Date();
        return t0.valueOf()
    }

    function finishSearch(){
        var t1 = new Date();
        return t1.valueOf()
    }
}
