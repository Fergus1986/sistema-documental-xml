#ifndef QUESTIONUI_H
#define QUESTIONUI_H

#include <QObject>

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

class QuestionUI: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString source
               READ source
               WRITE setSource
               NOTIFY sourceChanged)
public:
    QuestionUI();
    Q_INVOKABLE void read();

public slots:
    void setSource(const QString& source);
    QString source();

signals:
    void sourceChanged(const QString& source);
    void error(const QString& msg);

private:
    QString mSource;
};

#endif // QUESTIONUI_H
