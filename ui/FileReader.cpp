#include "FileReader.h"

FileReader::FileReader()
{
}

QString FileReader::read()
{
    QFile file(mSource);
    QString fileContent;
    if ( file.open(QIODevice::ReadOnly | QIODevice::Text) ) {

        fileContent=file.readAll();
        file.close();
    } else {
        emit error("Unable to open the file");
        return QString();
    }
    return fileContent;
}

void FileReader::setSource(const QString &source)
{
    this->mSource = source;
}

QString FileReader::source()
{
    return mSource;
}
