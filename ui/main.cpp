#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"

#include <QtQml>

#include "QuestionUI.h"
#include "QuestionDocumentUI.h"
#include "FileReader.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<QuestionUI>("QuestionUI", 1, 0, "QuestionUI");
    qmlRegisterType<QuestionDocumentUI>("QuestionDocumentUI", 1, 0, "QuestionDocumentUI");
    // qmlRegisterType<FileReader>("FileReader", 1, 0, "FileReader");

    QtQuick2ApplicationViewer viewer;
    viewer.setMainQmlFile(QStringLiteral("qml/qml/main.qml"));
    viewer.showFullScreen();

    return app.exec();
}
