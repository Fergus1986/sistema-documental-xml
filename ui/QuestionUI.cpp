#include "QuestionUI.h"

QuestionUI::QuestionUI()
{
}

void QuestionUI::read()
{
    if (mSource.isEmpty()){
        std::cout << "source is empty" << std::endl;
    }

    QString query=mSource;
    pid_t pid;
    pid=fork();

    if(pid==-1){
        std::cerr << "Error al crear el proceso hijo\n" << std::endl;
        exit(0);
    }
    if(pid){
        //Proceso padre
        std::cout << "Soy el padre el pid de mi hijo es << " << pid << std::endl;

        waitpid(pid, NULL, 0);

    }else{
        std::cout << "Soy el hijo" << std::endl;
        execl("./search",
              "search",
              query.toStdString().data(),
              "../BBDD/Posting/",
              "../BBDD/Index/",
              "../BBDD/StopList/StopList-En.txt",
              "../output",
              NULL);
    }
}

void QuestionUI::setSource(const QString &source)
{
    mSource = source;

}

QString QuestionUI::source()
{
    return mSource;
}
