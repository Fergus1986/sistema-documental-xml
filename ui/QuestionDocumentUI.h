#ifndef QUESTIONDOCUMENTUI_H
#define QUESTIONDOCUMENTUI_H

#include <QObject>

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <DocumentXML.h>

class QuestionDocumentUI: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString source
               READ source
               WRITE setSource
               NOTIFY sourceChanged)
public:
    QuestionDocumentUI();
    Q_INVOKABLE void read();

public slots:
    void setSource(const QString& source);
    QString source();

signals:
    void sourceChanged(const QString& source);
    void error(const QString& msg);

private:
    QString mSource;
};

#endif // QUESTIONDOCUMENTUI_H
