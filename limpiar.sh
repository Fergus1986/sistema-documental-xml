#!/bin/bash

#Uso: sh index.sh <directorio entrada>

function ayuda {
  echo " Este script limpia toda la BD"
  echo " Uso: ./limpiar.sh"
  echo "================================================================"
}

#Variables
INDICES=BBDD/Index/*.xml
POSTING=BBDD/Posting/*.xml

INICIO=$(date +"%s")
rm -f $INDICES $POSTING
FIN=$(date +"%s")

TIEMPO=$(($FIN-$INICIO))

echo "La limpieza de la BD ha tardado $(($TIEMPO / 60)) minutos y $(($TIEMPO % 60)) segundos."

