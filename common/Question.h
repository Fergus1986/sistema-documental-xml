#ifndef QUESTION_H
#define QUESTION_H

#include <QString>
#include <QStringList>
#include <StopList.h>

#include "Document.h"

class Question: public Document
{
public:
    Question();
    Question(const QString& query, StopList* stopList);
    ~Question();

protected:
    void count(const QStringList& query);
    void generateID();

private:
    QByteArray data;
    QStringList queryTerms;
};

#endif // QUESTION_H
