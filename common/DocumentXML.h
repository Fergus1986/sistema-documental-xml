#ifndef DOCUMENTXML_H
#define DOCUMENTXML_H

#include <QString>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "Document.h"

class DocumentXML
{
public:
    DocumentXML();
    DocumentXML(Document &doc, const QString& path);
    DocumentXML(const QString& name);
    ~DocumentXML();

    Document* getDocument();

protected:
    void writeDocument();
    void readDocument();
    void readWords();

private:
    QString XMLname;
    QString XMLpath;
    QFile file;
    Document* doc;
    QXmlStreamWriter xmlWriter;
    QXmlStreamReader xmlReader;
};

#endif // DOCUMENTXML_H
