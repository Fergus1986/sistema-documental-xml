#ifndef RESULT_H
#define RESULT_H

#include <QString>
#include <QMap>
#include <QPair>

#include "Document.h"

class Result
{
public:
    Result();
    Result(Document* question);
    ~Result();

    QString getQuestionName();

    void insertDocument(float relevance, const QString& documentID, const QString& documentName);
    QMap<float, QString> getSimilarityTable();
    QMap<QString, QString> getDocumentsTable();

private:
    Document* question;
    QMap<float, QString> similarity;
    QMap<QString, QString> documents;

};

#endif // RESULT_H
