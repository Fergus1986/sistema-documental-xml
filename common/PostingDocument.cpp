#include "PostingDocument.h"

PostingDocument::PostingDocument()
{
}

PostingDocument::~PostingDocument()
{
}

void PostingDocument::setName(const QString &name)
{
    this->_name=name;
}

QString PostingDocument::getName()
{
    return this->_name;
}

QList<QString> PostingDocument::getStoreDocuments()
{
    return _documents;
}

int PostingDocument::insertDocument(const QString &documentID)
{
    _documents.append(documentID);
    int result = _documents.indexOf(documentID);
    return result;
}

QString PostingDocument::getDocumentID(int documentIndex)
{
    QString result = _documents.at(documentIndex);

    return result;
}

