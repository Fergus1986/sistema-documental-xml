#ifndef STOPLIST_H
#define STOPLIST_H

#include <QFile>
#include <QString>
#include <QStringList>
#include <QTextStream>

#include <iostream>


class StopList
{
public:
    StopList();
    StopList(const QString& path);
    ~StopList();

    const QStringList getList();

protected:
    void generate();

private:
    QFile descriptor;
    QStringList list;

};

#endif // STOPLIST_H
