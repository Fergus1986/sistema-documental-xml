#include "PostingFile.h"

PostingFile::PostingFile()
{
}

PostingFile::~PostingFile()
{
}

void PostingFile::setName(const QString &name)
{
    this->name = name;
}

QString PostingFile::getName()
{
    return this->name;
}

QMultiMap<QString, PostingInfo> &PostingFile::getPostingFile()
{
    return this->_postingFile;
}

void PostingFile::insertDocument(Document *document, int smallID)
{
    QMap<QString, float>::const_iterator it = document->getRelevanceTable().constBegin();
    while (it != document->getRelevanceTable().constEnd()) {
        insertWord(it.key(), smallID, it.value());
        ++it;
    }
}

void PostingFile::insertWord(const QString &word, int document, float frecuency)
{
    _postingFile.insert(word, PostingInfo(document, frecuency));
}

QList<QString> PostingFile::getStoreWords()
{
    return _postingFile.uniqueKeys();
}

QList<PostingInfo> PostingFile::findWord(const QString &word)
{
    QList<PostingInfo> result;
    result = _postingFile.values(word);
    return result;
}

