#include "Question.h"

Question::Question()
{
}

Question::Question(const QString &query, StopList *stopList)
{
    queryTerms = query.split(QRegExp("\\s"), QString::SkipEmptyParts);

    // Hash for ID
    data.append(query);

    setName(query);
    setPath("");
    setTotalWords(0);
    setRelevantWords(0);
    setMaxFr(0);

    setStopList(stopList);
    this->count(queryTerms);
    this->generateID();
}

Question::~Question()
{
}

void Question::count(const QStringList &query)
{
    QString queryTerm;
    QString word;
    float relevance;
    QRegExp rxWord("\\w+");
    QRegExp rxWeight("\\w+\\:(1|0\\.\\d+)");

    for(int i=0; i<query.size(); i++){
        queryTerm = query.at(i);
        relevance=0;

        if(rxWord.exactMatch(queryTerm) | rxWeight.exactMatch(queryTerm)){

            if(rxWord.exactMatch(queryTerm)){
                // Syntaxis-> Word
                word = queryTerm;
                relevance = 1;
            }else{
                // Syntaxis -> Word:Weight
                word = queryTerm.split(":").at(0);
                relevance = queryTerm.split(":").at(1).toFloat();
            }

            // Is word?
            if(isWord(word)){
                this->incrementTotalWords();
                word = word.toLower();

                // Is relevant?
                if(isRelevant(word)){
                    this->incrementRelevantWords();
                    insertWord(word, relevance);
//                    std::cout << "w: " << word.toStdString() << ", " << "r: " << relevance << std::endl;

                    // Hash for ID
                    data.append(word);
                    data.append(":");
                    data.append(QString::number(relevance));
                }
            }
        }
    }
}

void Question::generateID()
{
    QByteArray hash = QCryptographicHash::hash(data,QCryptographicHash::Sha256);

    // Hash to String
    this->setID(hash.toHex().data());
}
