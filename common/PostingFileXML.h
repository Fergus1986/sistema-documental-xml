#ifndef POSTINGFILEXML_H
#define POSTINGFILEXML_H

#include <QString>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "PostingFile.h"

class PostingFileXML
{
public:
    PostingFileXML();
    PostingFileXML(const QString& path, const QString& name, PostingFile *posting);
    PostingFileXML(const QString& path, const QString& name);
    ~PostingFileXML();

    PostingFile* getPostingFile();

protected:
    void writeDocument();
    void readDocument();
    void readWords();

private:
    QString XMLpath;
    QString XMLname;
    QFile file;
    PostingFile* _posting;
    QXmlStreamWriter xmlWriter;
    QXmlStreamReader xmlReader;
};

#endif // POSTINGFILEXML_H
