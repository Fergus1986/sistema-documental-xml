#ifndef RESULTXML_H
#define RESULTXML_H

#include <QString>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "Result.h"

class ResultXML
{
public:
    ResultXML();
    ResultXML(const QString& path, const QString& name, Result* result);
    ~ResultXML();

protected:
    void writeDocument();

private:
    QString XMLpath;
    QString XMLname;
    QFile file;
    Result* result;
    QXmlStreamWriter xmlWriter;
    QXmlStreamReader xmlReader;
};

#endif // RESULTXML_H
