#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QFile>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QMap>
#include <QCryptographicHash>
#include <qmath.h>

#include <StopList.h>

#include <iostream>

class Document
{
public:
    Document();
    Document(const QString& name, StopList *stopList);
    Document(const QStringList& query, StopList *stopList);
    ~Document();

    QString getName();
    void setName(const QString& name);

    QString getPath();
    void setPath(const QString& path);

    QString getID();
    void setID(const QString& ID);

    int getTotalWords();
    void setTotalWords(int totalWords);
    void incrementTotalWords(int increment=1);

    int getRelevantWords();
    void setRelevantWords(int relevantWords);
    void incrementRelevantWords(int increment=1);

    int getStoreWords();

    void setMaxFr(int maxFr);
    int getMaxFr();

    void setStopList(StopList* StopList);

    void insertWord(const QString& word, float relevance=1);
    float findWord(const QString& word);
    QMap<QString, float> getRelevanceTable();

    void calculateRelevance();

    float calculateSimilarity(Document* document);

protected:
    void count(const QString& line);
    bool isWord(const QString& word);
    bool isRelevant(const QString& word);
    void generateID();

private:
    QString ID;
    QString name;
    QString path;
    QFile descriptor;
    QStringList stopList;
    int totalWords;
    int relevantWords;
    int _maxFr;
    QMap<QString, float> table;

};

#endif // DOCUMENT_H
