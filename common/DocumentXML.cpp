#include "DocumentXML.h"

DocumentXML::DocumentXML()
{
}

// Create a XML from a Doc
DocumentXML::DocumentXML(Document &doc, const QString &path)
{
    this->doc = &doc;
    XMLname = doc.getID();
    XMLpath = path + XMLname + ".xml";
    file.setFileName(XMLpath);

    if(!file.exists()){

        if (!file.open(QIODevice::WriteOnly)){
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be open"
                      << std::endl;
        }else{

            writeDocument();
            file.close();
            std::cout << XMLname.toStdString() << ".xml" << " created" << std::endl;
        }

    }else{
        std::cerr << "The file "
                  << file.fileName().toStdString()
                  << " already exist"
                  << std::endl;
    }
}

// Load a Doc XML from a file
DocumentXML::DocumentXML(const QString &name)
{
    XMLpath = name;
    file.setFileName(XMLpath);

    if(file.exists()){

        if (file.open(QIODevice::ReadOnly)){

            readDocument();
            file.close();

        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be open"
                      << std::endl;
        }

    }else{
        std::cerr << "The file "
                  << file.fileName().toStdString()
                  << " doesn't exist"
                  << std::endl;
    }

}

DocumentXML::~DocumentXML()
{
}

Document* DocumentXML::getDocument()
{
    return this->doc;
}

void DocumentXML::writeDocument()
{
    xmlWriter.setDevice(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("Document");
    xmlWriter.writeTextElement("ID", XMLname);
    xmlWriter.writeTextElement("Name", doc->getName());
    xmlWriter.writeTextElement("Store", QString::number(doc->getStoreWords()));
    xmlWriter.writeTextElement("Relevant", QString::number(doc->getRelevantWords()));
    xmlWriter.writeTextElement("Total", QString::number(doc->getTotalWords()));
    xmlWriter.writeTextElement("Max_Fr", QString::number(doc->getMaxFr()));
    xmlWriter.writeStartElement("Words");

    QMap<QString, float>::const_iterator it = doc->getRelevanceTable().constBegin();
    while (it != doc->getRelevanceTable().constEnd()) {
        xmlWriter.writeTextElement(it.key(), QString::number(it.value(), 'g', 4));
        ++it;
    }

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
}

void DocumentXML::readDocument()
{
    this->doc = new Document();

    xmlReader.setDevice(&file);
    while (!xmlReader.atEnd()) {

        if (xmlReader.hasError()) {
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " has a XML error"
                      << std::endl;
        }

        if(xmlReader.readNextStartElement()){
            if (xmlReader.name() == "ID"){
                doc->setID(xmlReader.readElementText());

            }else if(xmlReader.name()== "Name"){
                doc->setName(xmlReader.readElementText());

            }else if(xmlReader.name()== "Store"){

            }else if(xmlReader.name()== "Relevant"){
                doc->setRelevantWords(xmlReader.readElementText().toInt());

            }else if(xmlReader.name()== "Total"){
                doc->setTotalWords(xmlReader.readElementText().toInt());

            }else if(xmlReader.name()== "Max_Fr"){
                doc->setMaxFr(xmlReader.readElementText().toInt());

            }else if(xmlReader.name()== "Words"){
                readWords();
            }
        }
    }
}

void DocumentXML::readWords()
{
    while (!xmlReader.atEnd()) {
        if(xmlReader.readNextStartElement()){
            doc->insertWord(xmlReader.name().toString(), xmlReader.readElementText().toFloat());
        }
    }
}
