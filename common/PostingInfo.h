#ifndef POSTINGINFO_H
#define POSTINGINFO_H

#include <QString>

class PostingInfo
{
public:
    PostingInfo();
    PostingInfo(const int documentIndex, float relevance);
    ~PostingInfo();

    int getDocument();
    float getRelevance();

    bool operator ==(PostingInfo &object);


private:
    int _document;
    float _relevance;
};

#endif // POSTINGINFO_H
