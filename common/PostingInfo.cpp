#include "PostingInfo.h"

PostingInfo::PostingInfo()
{
}

PostingInfo::PostingInfo(const int documentIndex, float relevance)
{
    _document = documentIndex;
    _relevance = relevance;
}

PostingInfo::~PostingInfo()
{
}

int PostingInfo::getDocument()
{
    return _document;
}

float PostingInfo::getRelevance()
{
    return _relevance;
}

bool PostingInfo::operator ==(PostingInfo &object)
{
    return _document == object.getDocument();

}
