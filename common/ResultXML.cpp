#include "ResultXML.h"

ResultXML::ResultXML()
{
}

ResultXML::ResultXML(const QString &path, const QString &name, Result *result)
{
    XMLname = name;
    XMLpath = path + "/" + name + ".xml";
    this->result = result;
    file.setFileName(XMLpath);

    if(file.exists()){

        if (file.open(QIODevice::WriteOnly)){

            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " is going to be overwrited"
                      << std::endl;

            writeDocument();
            file.close();
            std::cout << XMLpath.toStdString() << " created" << std::endl;

        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be overwrited"
                      << std::endl;
        }

    }else{

        if (file.open(QIODevice::WriteOnly)){

            writeDocument();
            file.close();
            std::cout << XMLpath.toStdString() << " created" << std::endl;

        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be created"
                      << std::endl;
        }
    }
}

ResultXML::~ResultXML()
{
}

void ResultXML::writeDocument()
{
    QString title;

    xmlWriter.setDevice(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("Result");
    xmlWriter.writeTextElement("Name", XMLname);
    xmlWriter.writeTextElement("Path", XMLpath);
    xmlWriter.writeTextElement("Question", result->getQuestionName());
    xmlWriter.writeStartElement("Documents");
    xmlWriter.writeTextElement("Total", QString::number(result->getSimilarityTable().size()));

    QMapIterator<float, QString>itSim(result->getSimilarityTable());
    itSim.toBack();
    while(itSim.hasPrevious()){
        itSim.previous();
        xmlWriter.writeStartElement("Document");
        xmlWriter.writeTextElement("ID", itSim.value());
        title = result->getDocumentsTable().value(itSim.value());
        xmlWriter.writeTextElement("Title", title);
        xmlWriter.writeTextElement("Relevance", QString::number(itSim.key()));
        xmlWriter.writeEndElement();
    }

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
}
