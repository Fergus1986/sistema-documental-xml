#include "Document.h"

Document::Document()
{
}

Document::Document(const QString &name, StopList* stopList)
{
    totalWords = 0;
    relevantWords = 0;

    descriptor.setFileName(name);

    if(!descriptor.exists()){
        std::cerr << "The file "
                  << descriptor.fileName().toStdString()
                  << " doesn't exist"
                  << std::endl;

    }else{

        if(!descriptor.open(QIODevice::ReadOnly)){
            std::cerr << "Unable to open the file: "
                      << descriptor.fileName().toStdString()
                      << std::endl;

        }else{

            this->path = name;
            this->name = descriptor.fileName();
            this->stopList = stopList->getList();

            generateID();

            QString line;
            descriptor.seek(0);
            QTextStream read (&descriptor);
            while(!read.atEnd()){
                line=read.readLine();
                count(line);
            }
        }
    }

    // Closing File
    descriptor.close();
    this->calculateRelevance();
}

Document::~Document()
{
}

QString Document::getName()
{
    return this->name;
}

void Document::setName(const QString &name)
{
    this->name = name;
}

QString Document::getPath()
{
    return this->path;
}

void Document::setPath(const QString &path)
{
    this->path = path;
}

QString Document::getID()
{
    return this->ID;
}

void Document::setID(const QString &ID)
{
    this->ID=ID;
}

int Document::getTotalWords()
{
    return this->totalWords;
}

void Document::setTotalWords(int totalWords)
{
    this->totalWords=totalWords;
}

void Document::incrementTotalWords(int increment)
{
    this->totalWords+=increment;
}

int Document::getRelevantWords()
{
    return this->relevantWords;
}

void Document::setRelevantWords(int relevantWords)
{
    this->relevantWords=relevantWords;
}

void Document::incrementRelevantWords(int increment)
{
    this->relevantWords+=increment;
}

int Document::getStoreWords()
{
    return this->table.size();
}

void Document::setMaxFr(int maxFr)
{
    this->_maxFr=maxFr;
}

int Document::getMaxFr()
{
    return this->_maxFr;
}

void Document::setStopList(StopList *StopList)
{
    this->stopList=StopList->getList();
}

void Document::count(const QString &line)
{
    int i=0;
    QStringList list;
    QString word;

    list = line.split(QRegExp("\\W+"), QString::SkipEmptyParts);

    for(i=0; i<list.size(); i++){

        // Is word?
        if(isWord(list.at(i))){
            totalWords++;
            word=list.at(i).toLower();

            // Is relevant?
            if(isRelevant(word)){
                relevantWords++;
                insertWord(word);
            }
        }
    }
}

void Document::insertWord(const QString &word, float relevance)
{

    // Is new in the table?
    if(!table.contains(word)){
        table.insert(word, relevance);
    }else{
        float oldRelevance=0;
        oldRelevance = table.value(word) + 1;
        table.insert(word, oldRelevance);
    }
}

float Document::findWord(const QString &word)
{
    return table.value(word, 0);
}

QMap<QString, float> Document::getRelevanceTable()
{
    return this->table;
}

void Document::calculateRelevance()
{
    // Calculate tf = fr/max{fr}

    float tf=0;
    _maxFr =0;

    // Obtain max{fr}
    QMap<QString, float>::const_iterator it = table.constBegin();
    while (it != table.constEnd()) {
        if(it.value() >= _maxFr){
            _maxFr = it.value();
        }
        ++it;
    }

    // Save tf
    it = table.constBegin();
    QString word;
    while(it!= table.constEnd()){
        word = it.key();
        tf = it.value()/_maxFr;
        table.insert(word, tf);
        ++it;
    }
}

float Document::calculateSimilarity(Document *document)
{
    // Similarity
    // Document = w{i,j}
    // Question = w{i,q}
    // numerator = Sum(w{i,j}*w{i,q})
    // denominator = Sqrt(Sum(w{i,j}^2)*Sqrt(Sum(w{i,q}^2)

    float numerator=0.0;
    float denominator=0.0;

    QMap<QString, float>::const_iterator itDoc = table.constBegin();
    float weightProduct = 0.0;
    float weightSquare = 0.0;
    float weightThisTotal = 0.0;
    float weightOtherTotal = 0.0;
    while(itDoc!= table.end()){
//        std::cout << "n = " << itDoc.value() << "*" << document->findWord(itDoc.key()) << std::endl;
        weightProduct = itDoc.value() * document->findWord(itDoc.key());
        numerator += weightProduct;
        weightSquare = qPow(itDoc.value(), 2);
        weightThisTotal += weightSquare;
        ++itDoc;
    }

    QMap<QString, float>::const_iterator itOtherDoc = document->getRelevanceTable().constBegin();
    while(itOtherDoc != document->getRelevanceTable().constEnd()){
        weightSquare = qPow(itOtherDoc.value(), 2);
        weightOtherTotal += weightSquare;
        ++itOtherDoc;
    }

    denominator = qSqrt(weightThisTotal) * qSqrt(weightOtherTotal);

//    std::cout << "N: " << numerator << "\t" << denominator << "\t" << numerator/denominator << std::endl;

    return numerator/denominator;
}

bool Document::isWord(const QString &word)
{
    QChar character;
    bool value=false;

    character=word.at(0);
    if(character.isLetter()){
        value=true;
    }

    return value;
}

bool Document::isRelevant(const QString &word)
{
    bool value=true;
    if(stopList.contains(word)){
        value=false;
    }

    return value;
}


void Document::generateID()
{
    QByteArray data;
    QByteArray hash;

    data = descriptor.readAll();
    //        hash = QCryptographicHash::hash(data, QCryptographicHash::Md5);
    hash = QCryptographicHash::hash(data,QCryptographicHash::Sha256);

    // Hash to String
    ID.append(hash.toHex().data());
}
