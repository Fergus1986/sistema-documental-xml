#include "PostingFileXML.h"

PostingFileXML::PostingFileXML()
{
}

// Create a XML from a PostingFile
PostingFileXML::PostingFileXML(const QString &path, const QString &name, PostingFile* posting)
{
    XMLname = name;
    XMLpath = path + "/" + name + ".xml";
    _posting = posting;
    file.setFileName(XMLpath);

    if(file.exists()){

        if (file.open(QIODevice::WriteOnly)){

            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " is going to be overwrited"
                      << std::endl;

            writeDocument();
            file.close();
            std::cout << XMLpath.toStdString() << " created" << std::endl;

        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be overwrited"
                      << std::endl;
        }

    }else{

        if (file.open(QIODevice::WriteOnly)){

            writeDocument();
            file.close();
            std::cout << XMLpath.toStdString() << " created" << std::endl;

        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be created"
                      << std::endl;
        }
    }
}

// Load a PostingFile XML from a file
PostingFileXML::PostingFileXML(const QString &path, const QString &name)
{
    XMLname = name;
    XMLpath = path + "/" + name + ".xml";
    file.setFileName(XMLpath);

    if(file.exists()){

        if (file.open(QIODevice::ReadOnly)){

            readDocument();
            file.close();

        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be open"
                      << std::endl;
        }

    }else{
        std::cerr << "The file "
                  << file.fileName().toStdString()
                  << " doesn't exist"
                  << std::endl;
    }
}

PostingFileXML::~PostingFileXML()
{
}

PostingFile *PostingFileXML::getPostingFile()
{
    return _posting;
}

void PostingFileXML::writeDocument()
{

    QString word;
    QList<PostingInfo> values;
    QList<PostingInfo>::iterator it;

    int doc;
    float relevance;

    xmlWriter.setDevice(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("Document");
    xmlWriter.writeTextElement("Name", XMLname);
    xmlWriter.writeTextElement("Path", XMLpath);
    xmlWriter.writeTextElement("Store", QString::number(_posting->getStoreWords().size()));
    xmlWriter.writeStartElement("Words");

    for(int i=0; i<_posting->getStoreWords().size(); i++){
        word = _posting->getStoreWords().at(i);
        xmlWriter.writeStartElement(word);

        values = _posting->findWord(word);
        it = values.begin();

        while(it != values.end()){
            doc = (*it).getDocument();
            relevance = (*it).getRelevance();
            xmlWriter.writeAttribute("d_" + QString::number(doc), QString::number(relevance, 'g', 4));
            ++ it;
        }

        xmlWriter.writeEndElement();
    }

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
}

void PostingFileXML::readDocument()
{
    this->_posting = new PostingFile();

    xmlReader.setDevice(&file);
    while (!xmlReader.atEnd()) {

        if (xmlReader.hasError()) {
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " has a XML error"
                      << std::endl;
        }

        if(xmlReader.readNextStartElement()){
            if (xmlReader.name() == "Name"){
                _posting->setName(xmlReader.readElementText());

            }else if(xmlReader.name()== "Path"){
//                _posting->setPath(xmlReader.readElementText());

            }else if(xmlReader.name()== "Store"){
//                _posting->setStoreWords(xmlReader.readElementText().toInt());

            }else if(xmlReader.name()== "Words"){
                readWords();
            }
        }
    }

}

void PostingFileXML::readWords()
{
    QXmlStreamAttribute postingInfo;
    QString word;
    int doc;
    float relevance;
    while (!xmlReader.atEnd()) {
        if(xmlReader.readNextStartElement()){
            word = xmlReader.name().toString();
            for(int i=0; i< xmlReader.attributes().size(); i++){
                postingInfo = xmlReader.attributes().at(i);
                // Obtain the document number from the String "d_x"
                doc = postingInfo.name().toString().split("_").at(1).toInt();
                relevance = postingInfo.value().toString().toFloat();
                _posting->insertWord(word, doc, relevance);
            }
        }
    }
}
