#ifndef POSTINGFILE_H
#define POSTINGFILE_H

#include <QString>
#include <QMap>
#include <QMultiMap>

#include "PostingInfo.h"
#include "PostingDocument.h"
#include "Document.h"

class PostingFile
{
public:
    PostingFile();
    ~PostingFile();

    void setName(const QString& name);
    QString getName();

    QMultiMap<QString, PostingInfo> &getPostingFile();

    void insertDocument(Document* document, int smallID);
    void insertWord(const QString& word, int document, float frecuency);

    QList<QString> getStoreWords();
    QList<PostingInfo> findWord(const QString& word);

private:
    QString name;
    QMultiMap< QString, PostingInfo > _postingFile;
};

#endif // POSTINGFILE_H
