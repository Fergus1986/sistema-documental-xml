#include "PostingDocumentXML.h"

PostingDocumentXML::PostingDocumentXML()
{
}

// Create a XML from a PostingDocument
PostingDocumentXML::PostingDocumentXML(const QString& path, const QString &name, PostingDocument *postingDocument)
{
    XMLname = name;
    XMLpath = path + "/" + name + ".xml";
    _postingDocument = postingDocument;
    file.setFileName(XMLpath);

    if(file.exists()){

        if (file.open(QIODevice::WriteOnly)){

            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " is going to be overwrited"
                      << std::endl;

            writeDocument();
            file.close();
            std::cout << XMLpath.toStdString() << " created" << std::endl;

        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be overwrited"
                      << std::endl;
        }

    }else{

        if (file.open(QIODevice::WriteOnly)){

            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " is created"
                      << std::endl;

            writeDocument();
            file.close();
            std::cout << XMLpath.toStdString() << " created" << std::endl;

        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be created"
                      << std::endl;
        }
    }
}

// Load a XML from a file
PostingDocumentXML::PostingDocumentXML(const QString &path, const QString &name)
{
    XMLname = name;
    XMLpath = path + "/" + name + ".xml";
    file.setFileName(XMLpath);

    if(file.exists()){

        if (file.open(QIODevice::ReadOnly)){

            readDocument();
            file.close();


        }else{
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " can't be open"
                      << std::endl;
        }

    }else{
        std::cerr << "The file "
                  << file.fileName().toStdString()
                  << " doesn't exist"
                  << std::endl;
    }
}

PostingDocumentXML::~PostingDocumentXML()
{
}

PostingDocument *PostingDocumentXML::getPostingDocument()
{
    return _postingDocument;
}

void PostingDocumentXML::writeDocument()
{
    QString document;

    xmlWriter.setDevice(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("Document");
    xmlWriter.writeTextElement("Name", XMLname);
    xmlWriter.writeTextElement("Path", XMLpath);
    xmlWriter.writeTextElement("Store", QString::number(_postingDocument->getStoreDocuments().size()));
    xmlWriter.writeStartElement("Documents");

    for(int i=0; i<_postingDocument->getStoreDocuments().size(); i++){
        document = _postingDocument->getStoreDocuments().at(i);
        xmlWriter.writeTextElement("d_" + QString::number(i), document);
    }

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
}

void PostingDocumentXML::readDocument()
{
    this->_postingDocument = new PostingDocument();

    xmlReader.setDevice(&file);
    while (!xmlReader.atEnd()) {

        if (xmlReader.hasError()) {
            std::cerr << "The file "
                      << file.fileName().toStdString()
                      << " has a XML error"
                      << std::endl;
        }

        if(xmlReader.readNextStartElement()){
            if (xmlReader.name() == "Name"){
                _postingDocument->setName(xmlReader.readElementText());

            }else if(xmlReader.name()== "Store"){

            }else if(xmlReader.name()== "Documents"){
                readIndex();
            }
        }
    }
}

void PostingDocumentXML::readIndex()
{
    while (!xmlReader.atEnd()) {
        if(xmlReader.readNextStartElement()){
            _postingDocument->insertDocument(xmlReader.readElementText());
        }
    }
}
