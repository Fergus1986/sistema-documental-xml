#ifndef POSTINGDOCUMENT_H
#define POSTINGDOCUMENT_H

#include <QMap>
#include <QList>
#include <QString>

#include "Document.h"

class PostingDocument
{
public:
    PostingDocument();
    ~PostingDocument();

    void setName(const QString& name);
    QString getName();

    QList<QString> getStoreDocuments();

    int insertDocument(const QString& documentID);
    QString getDocumentID(int documentIndex);


private:
    QString _name;
    QList <QString> _documents;
};

#endif // POSTINGDOCUMENT_H
