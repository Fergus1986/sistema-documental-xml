#include "Result.h"

Result::Result()
{
}

Result::Result(Document *question)
{
    this->question = question;
}

Result::~Result()
{
}

QString Result::getQuestionName()
{
    return question->getID();
}

void Result::insertDocument(float relevance, const QString &documentID, const QString &documentName)
{
    similarity.insert(relevance, documentID);
    documents.insert(documentID, documentName);
}

QMap<float, QString> Result::getSimilarityTable()
{
    return this->similarity;
}

QMap<QString, QString> Result::getDocumentsTable()
{
    return this->documents;
}
