#ifndef POSTINGDOCUMENTXML_H
#define POSTINGDOCUMENTXML_H

#include <QString>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "PostingDocument.h"

class PostingDocumentXML
{
public:
    PostingDocumentXML();
    PostingDocumentXML(const QString& path, const QString& name, PostingDocument* postingDocument);
    PostingDocumentXML(const QString& path, const QString& name);
    ~PostingDocumentXML();

    PostingDocument* getPostingDocument();

protected:
    void writeDocument();
    void readDocument();
    void readIndex();

private:
    QString XMLname;
    QString XMLpath;
    QFile file;
    PostingDocument* _postingDocument;
    QXmlStreamWriter xmlWriter;
    QXmlStreamReader xmlReader;

};

#endif // POSTINGDOCUMENTXML_H
