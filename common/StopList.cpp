#include "StopList.h"

StopList::StopList()
{
}

StopList::StopList(const QString &path)
{
    this->descriptor.setFileName(path);

    if(!this->descriptor.exists()){
        std::cerr << "The file "
                  << this->descriptor.fileName().toStdString()
                  << " doesn't exist"
                  << std::endl;
    }else{
        if(!this->descriptor.open(QIODevice::ReadOnly | QIODevice::Text)){
            std::cerr << "Unable to open the file: "
                      << this->descriptor.fileName().toStdString()
                      << std::endl;
        }else{
            this->generate();
        }
    }
}

StopList::~StopList()
{
}

const QStringList StopList::getList()
{
    return this->list;
}

void StopList::generate()
{
    QString line;
    QTextStream read(&this->descriptor);

    while(!read.atEnd()){
//        std::cout << line.toStdString() << std::endl;
        line = read.readLine();
        list.append(line.split(QRegExp("\\W+"), QString::SkipEmptyParts));
    }
}


