#!/bin/bash

#Uso: sh anadir.sh <directorio entrada>

function ayuda {\
  echo " Este script anade un directorio para ser parseado"
  echo " Uso: ./anadir.sh <directorio entrada>"
  echo "================================================================"
}

if [ $# -lt 1 ]; then
  ayuda
exit 1
fi

#Variables
INDICES=BBDD/Index/
STOP=BBDD/StopList/StopList-En.txt
PARSER=build/parser
INDEX=index.sh

#parser <directorio de entrada> <directorio de salida> <stoplist>
INICIO=$(date +"%s")
$PARSER $1 $INDICES $STOP
FIN=$(date +"%s")

TIEMPO=$(($FIN-$INICIO))

echo "parser ha tardado $(($TIEMPO / 60)) minutos y $(($TIEMPO % 60)) segundos."

sh $INDEX
