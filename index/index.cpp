#include <iostream>

#include <QDir>

#include <QString>
#include <QStringList>

#include <qmath.h>

#include <DocumentXML.h>
#include <PostingFile.h>
#include <PostingFileXML.h>
#include <PostingDocument.h>
#include <PostingDocumentXML.h>


using namespace std;

int main(int argc, char *argv[])
{
    // First argument: Input folder
    // Second argument: Output folder

    if(argc != 3){
        cerr << "Number of parameters incorrect" << endl;
        return(1);
    }

    QDir input_folder(argv[1]);
    QDir output_folder(argv[2]);

    if(!input_folder.exists()){
        cerr << "Input folder doesn't exist" << endl;
        return(1);
    }

    if(!output_folder.exists()){
        cerr << "Output folder doesn't exist" << endl;
        return(1);
    }

    // Read the Index
    input_folder.setFilter(QDir::Files |QDir::NoSymLinks);
    input_folder.setSorting(QDir::Name);

    QFileInfoList fileList = input_folder.entryInfoList();
    QFileInfo file;

    int nFiles = fileList.size();
//    nFiles = 5;

    // Create the PostingDocument & PostingFile
    PostingFile postingFile;
    PostingDocument postingDocument;
    int smallID=0;

    for(int i=0; i<nFiles; i++){
        file=fileList.at(i);
        // Load XML file
        DocumentXML xml(file.absoluteFilePath());
        // Create PostingDocument and generate smallID
        smallID = postingDocument.insertDocument(xml.getDocument()->getID());
        postingFile.insertDocument(xml.getDocument(), smallID);
    }

    // Create PostingDocumentXML file
    PostingDocumentXML postingDocumentXML(output_folder.absolutePath(), "postingDocument", &postingDocument);

    // Create Alphabet PostingFiles
    QStringList alphabet;
    alphabet << "a" << "b" << "c" << "d" << "e" << "f" << "g" << "h" << "i";
    alphabet << "j" << "k" << "l" << "m" << "n" << "ñ" << "o" << "p" << "q";
    alphabet << "r" << "s" << "t" << "u" << "v" << "w" << "x" << "y" << "z";

    QList<PostingInfo> postingInfo;
    QList<QString> words = postingFile.getStoreWords();
    QString letter;
    QString word;
    int doc;

    // Calculate weight = tf * idf
    // Calculate idf = log(N/n{i})
    float weight=0;
    float tf=0;
    int nI=0;

    for(int i=0; i<alphabet.size(); i++){
        letter = alphabet.at(i);
        PostingFile* posting = new PostingFile();
        for(int j=0; j<words.size(); j++){
            word = words.at(j);

            // All words that start with the letter "letter"
            if(word.startsWith(letter)){
                postingInfo = postingFile.findWord(word);

                // All documents that have the term "word"
                nI = postingInfo.size();
                for(int k=0; k<nI; k++){
                    doc = postingInfo.value(k).getDocument();
                    tf = postingInfo.value(k).getRelevance();
                    // Calculate of weight
                    weight = tf * qLn(nFiles/(float)nI);
                    posting->insertWord(word, doc, weight);
                }
            }
        }

        // Create Alphabet PostingFileXML file
        PostingFileXML postingXML(output_folder.absolutePath(), letter, posting);
    }



//    ----------------------------------PRUEBAS---------------------------------------


//    PostingDocumentXML prueba (output_folder.absolutePath(), "postingDocument");



//    PostingFileXML prueba(output_folder.absolutePath(), "a");
//    for(int i=0; i<prueba.getPostingFile()->getStoreWords().size(); i++){
//        QString word = prueba.getPostingFile()->getStoreWords().at(i);
//        std::cout << word.toStdString() << std::endl;

//        QList<PostingInfo> values = prueba.getPostingFile()->findWord(word);
//        QList<PostingInfo>::iterator it = values.begin();

//        while(it != values.end()){
//            int doc = (*it).getDocument();
//            float relevance = (*it).getRelevance();
//            std::cout << "\t\t" << doc << ", " << relevance << std::endl;
//            ++ it;
//        }
//    }


//    for(int i=0; i<postingFile.getStoreWords().size(); i++){
//        QString word = postingFile.getStoreWords().at(i);
//        std::cout << word.toStdString() << std::endl;

//        QList<PostingInfo> values = postingFile.findWord(word);
//        QList<PostingInfo>::iterator it = values.begin();

//        while(it != values.end()){
//            int doc = (*it).getDocument();
//            float relevance = (*it).getRelevance();
//            std::cout << "\t\t" << doc << ", " << relevance << std::endl;
//            ++ it;
//        }
//    }

    cout << "Success!" << endl;
    return 0;
}

