#!/bin/bash

#Uso: sh index.sh <directorio entrada>

function ayuda {\
  echo " Este script genera los archivos de PostingFile a partir de los archivos índice de los documentos"
  echo " Uso: ./index.sh"
  echo "================================================================"
}

#Variables
INDICES=BBDD/Index/
POSTING=BBDD/Posting/
INDEX=build/index

#index <directorio de entrada> <directorio de salida>
INICIO=$(date +"%s")
$INDEX $INDICES $POSTING
FIN=$(date +"%s")

TIEMPO=$(($FIN-$INICIO))

echo "index ha tardado $(($TIEMPO / 60)) minutos y $(($TIEMPO % 60)) segundos."